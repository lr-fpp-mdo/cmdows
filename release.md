Instructions for releasing a new version of CMDOWS
==================================================

- Check that all relevant issues from GitLab are resolved 

- Check that all the files in the docs directory are up to date. This includes:

      - An updated XMIND document 
	  
	  - An updated PDF document (created based on the XMIND document)

- Create a new example XML file in the schema directory

- Update the examples in the example directory
	  
- Update the readme.md file. This mainly concerns:
	  
	  - The paths defined in "Repository Structure"
	  
	  - A new entry under "Changelog"

- Include the new schema in KADMOS and update the CMDOWS_VERSION variable in the main KadmosGraph class 