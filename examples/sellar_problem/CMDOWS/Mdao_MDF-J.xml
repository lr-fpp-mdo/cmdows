<?xml version='1.0' encoding='UTF-8'?>
<cmdows xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.9/cmdows.xsd">
  <header>
    <creator>Imco van Gent</creator>
    <description>Mdao CMDOWS file of the well-known Sellar problem</description>
    <timestamp>2023-03-24T15:36:59.753550</timestamp>
    <fileVersion>0.1</fileVersion>
    <cmdowsVersion>0.9</cmdowsVersion>
    <updates>
      <update>
        <modification>KADMOS export of a mdao data graph (MDG).</modification>
        <creator>Imco van Gent</creator>
        <timestamp>2023-03-24T15:36:59.753550</timestamp>
        <fileVersion>0.1</fileVersion>
        <cmdowsVersion>0.9</cmdowsVersion>
      </update>
    </updates>
    <organization>
      <contacts>
        <contact uID="ivangent">
          <name>Imco van Gent</name>
          <email>i.vangent@tudelft.nl</email>
          <company>TU Delft</company>
        </contact>
        <contact uID="lmuller">
          <name>Lukas Muller</name>
          <email>l.muller@student.tudelft.nl</email>
          <company>TU Delft</company>
        </contact>
      </contacts>
      <organigram>
        <architects>
          <architect>
            <contactUID>lmuller</contactUID>
          </architect>
          <architect>
            <contactUID>ivangent</contactUID>
          </architect>
        </architects>
        <integrators>
          <integrator>
            <contactUID>ivangent</contactUID>
          </integrator>
        </integrators>
      </organigram>
    </organization>
  </header>
  <executableBlocks>
    <mathematicalFunctions>
      <mathematicalFunction uID="D[2]">
        <label>D[2]</label>
        <functionType>regular</functionType>
        <inputs>
          <input>
            <parameterUID>/dataSchema/variables/z1</parameterUID>
            <equationLabel>z1</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/variables/z2</parameterUID>
            <equationLabel>z2</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y1</parameterUID>
            <equationLabel>y1</equationLabel>
          </input>
        </inputs>
        <outputs>
          <output>
            <parameterUID>/dataSchema/analyses/y2</parameterUID>
            <equations uID="D[2]_equation">
              <equation language="Python">abs(y1)**.5 + z1 + z2</equation>
            </equations>
          </output>
          <output>
            <parameterUID>/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y2</parameterUID>
            <equationsUID>D[2]_equation</equationsUID>
          </output>
        </outputs>
      </mathematicalFunction>
      <mathematicalFunction uID="D[1]">
        <label>D[1]</label>
        <functionType>regular</functionType>
        <inputs>
          <input>
            <parameterUID>/dataSchema/variables/x</parameterUID>
            <equationLabel>x</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/variables/z1</parameterUID>
            <equationLabel>z1</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/variables/z2</parameterUID>
            <equationLabel>z2</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y2</parameterUID>
            <equationLabel>y2</equationLabel>
          </input>
        </inputs>
        <outputs>
          <output>
            <parameterUID>/dataSchema/analyses/y1</parameterUID>
            <equations uID="D[1]_equation">
              <equation language="Python">z1**2. + x + z2 - .2*y2</equation>
            </equations>
          </output>
          <output>
            <parameterUID>/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y1</parameterUID>
            <equationsUID>D[1]_equation</equationsUID>
          </output>
        </outputs>
      </mathematicalFunction>
      <mathematicalFunction uID="G[2]">
        <label>G[2]</label>
        <functionType>regular</functionType>
        <inputs>
          <input>
            <parameterUID>/dataSchema/analyses/y2</parameterUID>
            <equationLabel>y2</equationLabel>
          </input>
        </inputs>
        <outputs>
          <output>
            <parameterUID>/dataSchema/analyses/g2</parameterUID>
            <equations uID="G[2]_equation">
              <equation language="Python">1-y2/24.0</equation>
            </equations>
          </output>
          <output>
            <parameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g2</parameterUID>
            <equationsUID>G[2]_equation</equationsUID>
          </output>
        </outputs>
      </mathematicalFunction>
      <mathematicalFunction uID="F">
        <label>F</label>
        <functionType>regular</functionType>
        <inputs>
          <input>
            <parameterUID>/dataSchema/variables/x</parameterUID>
            <equationLabel>x</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/analyses/y1</parameterUID>
            <equationLabel>y1</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/analyses/y2</parameterUID>
            <equationLabel>y2</equationLabel>
          </input>
          <input>
            <parameterUID>/dataSchema/variables/z1</parameterUID>
            <equationLabel>z1</equationLabel>
          </input>
        </inputs>
        <outputs>
          <output>
            <parameterUID>/dataSchema/analyses/f</parameterUID>
            <equations uID="F_equation">
              <equation language="Python">x**2+z1+y1+exp(-y2)</equation>
            </equations>
          </output>
          <output>
            <parameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/f</parameterUID>
            <equationsUID>F_equation</equationsUID>
          </output>
        </outputs>
      </mathematicalFunction>
      <mathematicalFunction uID="G[1]">
        <label>G[1]</label>
        <functionType>regular</functionType>
        <inputs>
          <input>
            <parameterUID>/dataSchema/analyses/y1</parameterUID>
            <equationLabel>y1</equationLabel>
          </input>
        </inputs>
        <outputs>
          <output>
            <parameterUID>/dataSchema/analyses/g1</parameterUID>
            <equations uID="G[1]_equation">
              <equation language="Python">y1/3.16-1</equation>
            </equations>
          </output>
          <output>
            <parameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g1</parameterUID>
            <equationsUID>G[1]_equation</equationsUID>
          </output>
        </outputs>
      </mathematicalFunction>
    </mathematicalFunctions>
  </executableBlocks>
  <parameters>
    <parameter uID="/dataSchema/analyses/f">
      <label>f</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/analyses/g1">
      <label>g1</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/analyses/g2">
      <label>g2</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/variables/x">
      <label>x</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/analyses/y1">
      <label>y1</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/analyses/y2">
      <label>y2</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/variables/z1">
      <label>z1</label>
      <instanceID>1</instanceID>
    </parameter>
    <parameter uID="/dataSchema/variables/z2">
      <label>z2</label>
      <instanceID>1</instanceID>
    </parameter>
  </parameters>
  <problemDefinition uID="MDFJacobi">
    <problemFormulation>
      <mdaoArchitecture>MDF</mdaoArchitecture>
      <convergerType>Jacobi</convergerType>
      <executableBlocksOrder>
        <executableBlockUID position="1">D[1]</executableBlockUID>
        <executableBlockUID position="2">D[2]</executableBlockUID>
        <executableBlockUID position="3">G[1]</executableBlockUID>
        <executableBlockUID position="4">G[2]</executableBlockUID>
        <executableBlockUID position="5">F</executableBlockUID>
      </executableBlocksOrder>
      <allowUnconvergedCouplings>false</allowUnconvergedCouplings>
    </problemFormulation>
    <problemRoles>
      <parameters>
        <designVariables>
          <designVariable uID="__desVar__/dataSchema/variables/x">
            <parameterUID>/dataSchema/variables/x</parameterUID>
            <nominalValue>2.0</nominalValue>
            <validRanges>
              <limitRange>
                <minimum>0.0</minimum>
                <maximum>10</maximum>
              </limitRange>
            </validRanges>
          </designVariable>
          <designVariable uID="__desVar__/dataSchema/variables/z1">
            <parameterUID>/dataSchema/variables/z1</parameterUID>
            <nominalValue>2.0</nominalValue>
            <validRanges>
              <limitRange>
                <minimum>-10</minimum>
                <maximum>10</maximum>
              </limitRange>
            </validRanges>
          </designVariable>
          <designVariable uID="__desVar__/dataSchema/variables/z2">
            <parameterUID>/dataSchema/variables/z2</parameterUID>
            <nominalValue>2.0</nominalValue>
            <validRanges>
              <limitRange>
                <minimum>0.0</minimum>
                <maximum>10</maximum>
              </limitRange>
            </validRanges>
          </designVariable>
        </designVariables>
        <objectiveVariables>
          <objectiveVariable uID="__objVar__/dataSchema/analyses/f">
            <parameterUID>/dataSchema/analyses/f</parameterUID>
          </objectiveVariable>
        </objectiveVariables>
        <constraintVariables>
          <constraintVariable uID="__conVar__/dataSchema/analyses/g1">
            <parameterUID>/dataSchema/analyses/g1</parameterUID>
            <constraintType>inequality</constraintType>
            <constraintOperator>&gt;=</constraintOperator>
            <referenceValue>0.0</referenceValue>
          </constraintVariable>
          <constraintVariable uID="__conVar__/dataSchema/analyses/g2">
            <parameterUID>/dataSchema/analyses/g2</parameterUID>
            <constraintType>inequality</constraintType>
            <constraintOperator>&gt;=</constraintOperator>
            <referenceValue>0.0</referenceValue>
          </constraintVariable>
        </constraintVariables>
      </parameters>
      <executableBlocks>
        <coupledBlocks>
          <coupledBlock>D[1]</coupledBlock>
          <coupledBlock>D[2]</coupledBlock>
        </coupledBlocks>
        <postCouplingBlocks>
          <postCouplingBlock>G[1]</postCouplingBlock>
          <postCouplingBlock>G[2]</postCouplingBlock>
          <postCouplingBlock>F</postCouplingBlock>
        </postCouplingBlocks>
      </executableBlocks>
    </problemRoles>
  </problemDefinition>
  <workflow>
    <problemDefinitionUID>MDFJacobi</problemDefinitionUID>
    <dataGraph>
      <name>XDSM - MDF-J</name>
      <edges>
        <edge>
          <fromExecutableBlockUID>D[1]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/analyses/y1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>D[1]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>D[2]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/analyses/y2</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>D[2]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y2</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>F</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/analyses/f</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>F</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/f</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>G[1]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/analyses/g1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>G[1]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>G[2]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/analyses/g2</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>G[2]</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g2</toParameterUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/f</fromParameterUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/g1</fromParameterUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/g2</fromParameterUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/x</fromParameterUID>
          <toExecutableBlockUID>D[1]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/x</fromParameterUID>
          <toExecutableBlockUID>F</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/y1</fromParameterUID>
          <toExecutableBlockUID>F</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/y1</fromParameterUID>
          <toExecutableBlockUID>G[1]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/y1</fromParameterUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/y2</fromParameterUID>
          <toExecutableBlockUID>F</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/y2</fromParameterUID>
          <toExecutableBlockUID>G[2]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/analyses/y2</fromParameterUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/z1</fromParameterUID>
          <toExecutableBlockUID>D[1]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/z1</fromParameterUID>
          <toExecutableBlockUID>D[2]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/z1</fromParameterUID>
          <toExecutableBlockUID>F</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/z2</fromParameterUID>
          <toExecutableBlockUID>D[1]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/variables/z2</fromParameterUID>
          <toExecutableBlockUID>D[2]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Coordinator</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/initialGuessCouplingVariables/dataSchemaCopy/analyses/y1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Coordinator</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/initialGuessCouplingVariables/dataSchemaCopy/analyses/y2</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Coordinator</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/x</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Coordinator</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/z1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Coordinator</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/z2</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y2</toParameterUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/initialGuessCouplingVariables/dataSchemaCopy/analyses/y1</fromParameterUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y1</fromParameterUID>
          <toExecutableBlockUID>D[2]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y1</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/initialGuessCouplingVariables/dataSchemaCopy/analyses/y2</fromParameterUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y2</fromParameterUID>
          <toExecutableBlockUID>D[1]</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y2</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/variables/x</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/x</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/variables/z1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/z1</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/variables/z2</toParameterUID>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toParameterUID>/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/z2</toParameterUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/x</fromParameterUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/x</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/z1</fromParameterUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/z1</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/z2</fromParameterUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/z2</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/f</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g1</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
        <edge>
          <fromParameterUID>/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g2</fromParameterUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
        </edge>
      </edges>
    </dataGraph>
    <processGraph>
      <name>mpg Sellar problem</name>
      <edges>
        <edge>
          <fromExecutableBlockUID>D[1]</fromExecutableBlockUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
          <processStepNumber>4</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>D[2]</fromExecutableBlockUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
          <processStepNumber>4</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>F</fromExecutableBlockUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
          <processStepNumber>6</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>G[1]</fromExecutableBlockUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
          <processStepNumber>6</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>G[2]</fromExecutableBlockUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
          <processStepNumber>6</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Coordinator</fromExecutableBlockUID>
          <toExecutableBlockUID>Optimizer</toExecutableBlockUID>
          <processStepNumber>1</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toExecutableBlockUID>D[1]</toExecutableBlockUID>
          <processStepNumber>3</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toExecutableBlockUID>D[2]</toExecutableBlockUID>
          <processStepNumber>3</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toExecutableBlockUID>G[1]</toExecutableBlockUID>
          <processStepNumber>5</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toExecutableBlockUID>G[2]</toExecutableBlockUID>
          <processStepNumber>5</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Converger</fromExecutableBlockUID>
          <toExecutableBlockUID>F</toExecutableBlockUID>
          <processStepNumber>5</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toExecutableBlockUID>Converger</toExecutableBlockUID>
          <processStepNumber>2</processStepNumber>
        </edge>
        <edge>
          <fromExecutableBlockUID>Optimizer</fromExecutableBlockUID>
          <toExecutableBlockUID>Coordinator</toExecutableBlockUID>
          <processStepNumber>7</processStepNumber>
        </edge>
      </edges>
      <nodes>
        <node>
          <referenceUID>D[1]</referenceUID>
          <processStepNumber>3</processStepNumber>
          <diagonalPosition>3</diagonalPosition>
        </node>
        <node>
          <referenceUID>D[2]</referenceUID>
          <processStepNumber>3</processStepNumber>
          <diagonalPosition>4</diagonalPosition>
        </node>
        <node>
          <referenceUID>F</referenceUID>
          <processStepNumber>5</processStepNumber>
          <diagonalPosition>7</diagonalPosition>
        </node>
        <node>
          <referenceUID>G[1]</referenceUID>
          <processStepNumber>5</processStepNumber>
          <diagonalPosition>5</diagonalPosition>
        </node>
        <node>
          <referenceUID>G[2]</referenceUID>
          <processStepNumber>5</processStepNumber>
          <diagonalPosition>6</diagonalPosition>
        </node>
        <node>
          <referenceUID>Coordinator</referenceUID>
          <processStepNumber>0</processStepNumber>
          <convergerStepNumber>7</convergerStepNumber>
          <diagonalPosition>0</diagonalPosition>
        </node>
        <node>
          <referenceUID>Converger</referenceUID>
          <processStepNumber>2</processStepNumber>
          <convergerStepNumber>4</convergerStepNumber>
          <diagonalPosition>2</diagonalPosition>
        </node>
        <node>
          <referenceUID>Optimizer</referenceUID>
          <processStepNumber>1</processStepNumber>
          <convergerStepNumber>6</convergerStepNumber>
          <diagonalPosition>1</diagonalPosition>
        </node>
      </nodes>
      <metadata>
        <loopNesting>
          <loopElements>
            <loopElement relatedUID="Coordinator">
              <loopElements>
                <loopElement relatedUID="Optimizer">
                  <loopElements>
                    <loopElement relatedUID="Converger">
                      <functionElements>
                        <functionElement>D[1]</functionElement>
                        <functionElement>D[2]</functionElement>
                      </functionElements>
                    </loopElement>
                  </loopElements>
                  <functionElements>
                    <functionElement>F</functionElement>
                    <functionElement>G[1]</functionElement>
                    <functionElement>G[2]</functionElement>
                  </functionElements>
                </loopElement>
              </loopElements>
            </loopElement>
          </loopElements>
        </loopNesting>
      </metadata>
    </processGraph>
  </workflow>
  <architectureElements>
    <parameters>
      <initialGuessCouplingVariables>
        <initialGuessCouplingVariable uID="/dataSchema/architectureNodes/initialGuessCouplingVariables/dataSchemaCopy/analyses/y1">
          <relatedParameterUID>/dataSchema/analyses/y1</relatedParameterUID>
          <label>y1^{c0}</label>
          <instanceID>1</instanceID>
        </initialGuessCouplingVariable>
        <initialGuessCouplingVariable uID="/dataSchema/architectureNodes/initialGuessCouplingVariables/dataSchemaCopy/analyses/y2">
          <relatedParameterUID>/dataSchema/analyses/y2</relatedParameterUID>
          <label>y2^{c0}</label>
          <instanceID>1</instanceID>
        </initialGuessCouplingVariable>
      </initialGuessCouplingVariables>
      <finalCouplingVariables>
        <finalCouplingVariable uID="/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y1">
          <relatedParameterUID>/dataSchema/analyses/y1</relatedParameterUID>
          <label>y1^{*}</label>
          <instanceID>1</instanceID>
        </finalCouplingVariable>
        <finalCouplingVariable uID="/dataSchema/architectureNodes/finalCouplingVariables/dataSchemaCopy/analyses/y2">
          <relatedParameterUID>/dataSchema/analyses/y2</relatedParameterUID>
          <label>y2^{*}</label>
          <instanceID>1</instanceID>
        </finalCouplingVariable>
      </finalCouplingVariables>
      <couplingCopyVariables>
        <couplingCopyVariable uID="/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y1">
          <relatedParameterUID>/dataSchema/analyses/y1</relatedParameterUID>
          <label>y1^{c}</label>
          <instanceID>1</instanceID>
        </couplingCopyVariable>
        <couplingCopyVariable uID="/dataSchema/architectureNodes/couplingCopyVariables/dataSchemaCopy/analyses/y2">
          <relatedParameterUID>/dataSchema/analyses/y2</relatedParameterUID>
          <label>y2^{c}</label>
          <instanceID>1</instanceID>
        </couplingCopyVariable>
      </couplingCopyVariables>
      <initialGuessDesignVariables>
        <initialGuessDesignVariable uID="/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/x">
          <relatedParameterUID>/dataSchema/variables/x</relatedParameterUID>
          <label>x^{0}</label>
          <instanceID>1</instanceID>
        </initialGuessDesignVariable>
        <initialGuessDesignVariable uID="/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/z1">
          <relatedParameterUID>/dataSchema/variables/z1</relatedParameterUID>
          <label>z1^{0}</label>
          <instanceID>1</instanceID>
        </initialGuessDesignVariable>
        <initialGuessDesignVariable uID="/dataSchema/architectureNodes/initialGuessDesignVariables/dataSchemaCopy/variables/z2">
          <relatedParameterUID>/dataSchema/variables/z2</relatedParameterUID>
          <label>z2^{0}</label>
          <instanceID>1</instanceID>
        </initialGuessDesignVariable>
      </initialGuessDesignVariables>
      <finalDesignVariables>
        <finalDesignVariable uID="/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/x">
          <relatedParameterUID>/dataSchema/variables/x</relatedParameterUID>
          <label>x^{*}</label>
          <instanceID>1</instanceID>
        </finalDesignVariable>
        <finalDesignVariable uID="/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/z1">
          <relatedParameterUID>/dataSchema/variables/z1</relatedParameterUID>
          <label>z1^{*}</label>
          <instanceID>1</instanceID>
        </finalDesignVariable>
        <finalDesignVariable uID="/dataSchema/architectureNodes/finalDesignVariables/dataSchemaCopy/variables/z2">
          <relatedParameterUID>/dataSchema/variables/z2</relatedParameterUID>
          <label>z2^{*}</label>
          <instanceID>1</instanceID>
        </finalDesignVariable>
      </finalDesignVariables>
      <finalOutputVariables>
        <finalOutputVariable uID="/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/f">
          <relatedParameterUID>/dataSchema/analyses/f</relatedParameterUID>
          <label>f^{*}</label>
          <instanceID>1</instanceID>
        </finalOutputVariable>
        <finalOutputVariable uID="/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g1">
          <relatedParameterUID>/dataSchema/analyses/g1</relatedParameterUID>
          <label>g1^{*}</label>
          <instanceID>1</instanceID>
        </finalOutputVariable>
        <finalOutputVariable uID="/dataSchema/architectureNodes/finalOutputVariables/dataSchemaCopy/analyses/g2">
          <relatedParameterUID>/dataSchema/analyses/g2</relatedParameterUID>
          <label>g2^{*}</label>
          <instanceID>1</instanceID>
        </finalOutputVariable>
      </finalOutputVariables>
    </parameters>
    <executableBlocks>
      <coordinators>
        <coordinator uID="Coordinator">
          <label>COOR</label>
        </coordinator>
      </coordinators>
      <optimizers>
        <optimizer uID="Optimizer">
          <label>OPT</label>
          <settings>
            <package>SciPy</package>
            <algorithm>SLSQP</algorithm>
            <maximumIterations>1000</maximumIterations>
            <maximumFunctionEvaluations>1000</maximumFunctionEvaluations>
            <convergenceTolerance>1e-06</convergenceTolerance>
            <constraintTolerance>1e-06</constraintTolerance>
            <applyScaling>true</applyScaling>
          </settings>
          <designVariables>
            <designVariable>
              <designVariableUID>__desVar__/dataSchema/variables/x</designVariableUID>
            </designVariable>
            <designVariable>
              <designVariableUID>__desVar__/dataSchema/variables/z1</designVariableUID>
            </designVariable>
            <designVariable>
              <designVariableUID>__desVar__/dataSchema/variables/z2</designVariableUID>
            </designVariable>
          </designVariables>
          <objectiveVariables>
            <objectiveVariable>
              <objectiveVariableUID>__objVar__/dataSchema/analyses/f</objectiveVariableUID>
            </objectiveVariable>
          </objectiveVariables>
          <constraintVariables>
            <constraintVariable>
              <constraintVariableUID>__conVar__/dataSchema/analyses/g1</constraintVariableUID>
            </constraintVariable>
            <constraintVariable>
              <constraintVariableUID>__conVar__/dataSchema/analyses/g2</constraintVariableUID>
            </constraintVariable>
          </constraintVariables>
        </optimizer>
      </optimizers>
      <convergers>
        <converger uID="Converger">
          <label>CONV</label>
          <settings>
            <linearSolver>
              <method>Jacobi</method>
              <lastIterationsToConsider>1</lastIterationsToConsider>
              <maximumIterations>100</maximumIterations>
              <convergenceToleranceRelative>1e-06</convergenceToleranceRelative>
              <convergenceToleranceAbsolute>1e-06</convergenceToleranceAbsolute>
            </linearSolver>
            <nonlinearSolver>
              <method>Jacobi</method>
              <lastIterationsToConsider>1</lastIterationsToConsider>
              <maximumIterations>100</maximumIterations>
              <convergenceToleranceRelative>1e-06</convergenceToleranceRelative>
              <convergenceToleranceAbsolute>1e-06</convergenceToleranceAbsolute>
            </nonlinearSolver>
          </settings>
        </converger>
      </convergers>
      <coupledAnalyses>
        <coupledAnalysis>
          <relatedExecutableBlockUID>D[1]</relatedExecutableBlockUID>
        </coupledAnalysis>
        <coupledAnalysis>
          <relatedExecutableBlockUID>D[2]</relatedExecutableBlockUID>
        </coupledAnalysis>
      </coupledAnalyses>
      <postCouplingAnalyses>
        <postCouplingAnalysis>
          <relatedExecutableBlockUID>F</relatedExecutableBlockUID>
        </postCouplingAnalysis>
        <postCouplingAnalysis>
          <relatedExecutableBlockUID>G[1]</relatedExecutableBlockUID>
        </postCouplingAnalysis>
        <postCouplingAnalysis>
          <relatedExecutableBlockUID>G[2]</relatedExecutableBlockUID>
        </postCouplingAnalysis>
      </postCouplingAnalyses>
    </executableBlocks>
  </architectureElements>
</cmdows>
