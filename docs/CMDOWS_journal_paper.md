CMDOWS has been published with open access in the [CEAS Aeronautical Journal](https://link.springer.com/article/10.1007/s13272-018-0307-2)

If you use CMDOWS in your work, please cite this journal article:
van Gent, I., La Rocca, G. & Hoogreef, M.F.M. CEAS Aeronaut J (2018) 9: 607. https://doi.org/10.1007/s13272-018-0307-2
