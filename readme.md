Introduction
=============

CMDOWS (Common MDO Workflow Schema) is a data definition for MDAO systems based on XML. CMDOWS is the main data exchanges format for the new [KADMOS](https://gitlab.tudelft.nl/lr-fpp-mdo/kadmos) software system which was developed in the [AGILE](http://www.agile-project.eu/) innovation project.

The CMDOWS data definition has the following objectives:
- Provide a clear separation between MDAO model formulations (KADMOS) and MDAO executable workflows (RCE, Optimus, etc).
- Provide the ability to link the same MDAO model formulation to different workflow softwares.
- Automatically instantiate executable workflows in [OpenMDAO](https://github.com/OpenMDAO/OpenMDAO) through the [OpenLEGO package](https://github.com/daniel-de-vries/OpenLEGO).
- Provide visualizations of standardized MDAO model formulations with VISTOMS (Visualization Tool for MDO Systems).

CMDOWS can store complete Repository Connectivity Graphs (RCGs), Fundamental Problem Graphs (FPGs) as well as MDAO Data/Process Graphs (MDG/MPG).
A simple browser interface for opening and visualizing CMDOWS files with VISTOMS is available at [mdo-system-interface.agile-project.eu](http://mdo-system-interface.agile-project.eu). This interface can also be started locally from the KADMOS package.


References
==========

CMDOWS has been published with open access in the [CEAS Aeronautical Journal](https://link.springer.com/article/10.1007/s13272-018-0307-2)

If you use CMDOWS in your work, please cite this journal article:
van Gent, I., La Rocca, G. & Hoogreef, M.F.M. CEAS Aeronaut J (2018) 9: 607. https://doi.org/10.1007/s13272-018-0307-2


Repository Structure
====================

The repository is structured as follows:

- docs/

      contains the documentation in different formats

- examples/

      contains example files

- schema/

      contains all major versions of CMDOWS as XSD and XML example file

      - 0.9/

         contains the current version of CMDOWS and can be directly accessed via [https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.7/cmdows.xsd](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.7/cmdows.xsd)

- license.md

      contains the license

- readme.md

      contains this document


Credits
=======

CMDOWS was developed at [TU Delft](https://tudelft.nl) by as an open-source project. CMDOWS can still be considered as an early beta and is subjected to change. Ideas and improvement suggestions are greatly appreciated!
There is an intended resemblance with the CPACS data exchange format for aircraft configurations which originates from the [DLR](https://software.dlr.de/p/cpacs/home/).


Changelog
=========

## [0.9](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.9) (12/03/2019)

- Adjusted some incorrect types (e.g. int for maxIterations instead of float)
- Changed naming of preIterator- and postIteratorAnalysis to uncoupledDesVarInd- and uncoupledDesVarDepAnalysis
- Enforced use of contactUID for all contacts throughout the schema
- Added scope attribute to validRanges
- Added sleepTime to mathematical functions
- Schema updates for the correct implementation of the BLISS-2000 approach
- Removed sampler and SM builder
- Updated settings and metadata for surrogate model element
- Created distributed system converger element
- Added new settings to "doe/settings": "centerRuns" and "levels"
- Changed names of "doe/settings" subelements (doeSeed -> seed, doeRuns -> runs, etc.)
- Added "package" element to "optimizer/settings" element
- Added complexType "solverSettingsType" for "linearSolver" and "nonlinearSolver"
- Adjusted "metadata" of "converger" element to contain "linearSolver" and "nonlinearSolver" settings
- Added "executableBlocksOrder" to "processGraph/metadata"
- Added optional "schemaPath" to "simpleArchitectureElementsVariableType" to allow for the use of uuid for all parameters

## [0.8](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.8) (13/04/18)

- Changed "xs:all" into "xs:sequence" in "simpleArchitectureElementsVariableType" to enable "xs:choice" between 'relatedParameterUID', 'relatedInstanceUID'
- Changed "xs:all" into "xs:sequence" in "executableBlocks/designCompetences/designCompetence/" to enable "xs:choice" between 'relatedInstaceUID' and 'ID', 'modeID', 'version', 'metadata', 'projectSpecific'
- Changed "xs:all" into "xs:sequence" in "parameters/parameter/" to enable "xs:choice" between 'relatedInstanceUID' and 'description', 'schemaPath', 'note', 'unit', 'dataType'
- Added "functionType" to "executableBlocks/mathematicalFunctions/mathematicalFunction/" can be specified as 'regular' or 'consistency'
- Replaced xs:float by xs:decimal under "architectureElements/executableBlocks/does/doe/settings/doeTable/tableRow/tableElement" to allow larger floating point precision
- Added optional "doeRuns" and "doeSeed" to "architectureElements/executableBlocks/does/doe/settings"
- Added "optimizerType" and "convergerType" to allow for new 'optimizer and 'converger' settings
- Added "lastIterationsToConsider", "maximumIterations", "algorithm", "applyScaling", "maximumFunctionEvaluations", "constraintTolerance", "convergenceTolerance", "convergenceToleranceRelative" and "convergenceToleranceAbsolute" to "simpleArchitectureElementBlockType/settings/"
- Change type to xs:anyType under "problemDefinition/problemRoles/parameters/constraintVariables/constraintVariable/referenceValue" to allow multiple bounds to be defined
- Added "relatedDesignCompetenceID" and "relatedDesignCompetenceVersion" to "executableBlocks/designCompetences/designCompetence/metadata/generalInfo/modelDefinition/" to enable shorter upload files
- Added "relatedInstanceUID" and "instanceID" to "simpleArchitectureElementsVariableType" to enable shorter upload files
- Added "schemaPath", "relatedInstanceUID" and "instanceID" to "parameters/parameter/" to enable shorter upload files
- Removed "instances" from "architectureElements/parameters/" replaced by local instance saving
- Added "copyDesignVariables", "surrogateModelApproximates" and "couplingWeights" to "architectureElements/parameters/" for consistency with CO and BLISS-2000
- Removed "consistencyConstraintFunstions" from "architectureElements/executableBlocks/" replaced by mathematical functions
- Added "surrogateModels", "boundaryDeterminators" and "surrogateModelBuilders" to "architectureElements/executableBlocks/"

## [0.7](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.7) (08/09/17)

- Added "organization" to "header" for defining an (AGILE) project organization
- Changed the structure of the "settings" element under "architectureElements/executableBlocks/does/doe"
- Replaced some elements of type xs:sequence with equivalent elements of type xs:all to allow for more flexibility
- Added "webAuthorizationType" element to "executableBlocks/designCompetences/designCompetence/metadata/executionInfo/remoteComponentInfo/dataExchangeSettings/"
- Removed "inputsType" complex type and replaced all occurences with more specific elements
- Made "executableBlocks/designCompetences/designCompetence/metadata/" an optional element
- Made elements under "executableBlocks/designCompetences/designCompetence/metadata/executionInfo/remoteComponentInfo/jobSettings" optional
- Replaced "lowerBound" and "upperBound" by "validRanges" element under "problemDefinition/parameters/designVariables/designVariable/"
- Replaced "projectspecific" with "projectSpecific" for consistency
- Changed the structure of the "loopNesting" dictionary under "workflow/processGraph/metadata" and therefore replaced the "dictType" by "loopElementType"
- Made "uID" attribute and "email" element in "contactType" optional for more flexibility

## [0.6](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.6) (10/08/17)

- Removed "version" and "type" from "executableBlocks/designCompetences/designCompetence/metadata/generalInfo"
- Added "ID", "modeID", "instanceID" and "version" to "executableBlocks/designCompetences/designCompetence/"
- Replaced "platformspecific" under "executableBlocks/designCompetences/designCompetence" with "projectspecific" and made it a free multi-purpose element (RCE, Optimus and other organizations may define their own subschema for this element)
- Added "remoteComponentInfo" under "executableBlocks/designCompetences/designCompetence/exectionInfo" with subelements (based on the deprecated "platformspecific" element)
- Changed the structure of the "output" element under "executableBlocks/mathematicalFunctions/outputs" in order to allow for referencing and using different languages (e.g. MathML or Python)
- Added "metadata" for future developments to "workflow/dataGraph" and to "workflow/processGraph"
- Added a "dictType" to store (nested) Python dictionaries
- Replaced "fromUID" and "toUID" under "workflow/processGraph/edges/edge" with "fromExecutableBlockUID" and "toExecutableBlockUID" respectively for clarity
- Replaced "fromUID" and "toUID" under "workflow/dataGraph/edges/edge" with choice "fromExecutableBlockUID"/"fromParameterUID" and choice "toExecutableBlockUID"/"toParameterUID" respectively for clarity
- Replaced "constraintType" by "constraintType" and "constraintOperator" under "problemDefinition/problemRoles/parameters/constraintVariables/constraintVariable"
- Set the "minOccurs" attribute to zero for "workflow/problemDefinitionUID" and "workflow/processGraph" so that the "workflow/dataGraph" can also be created for RCGs and FPGs
- Added "instanceID" to "architectureElements/parameters/intances/instance"

## [0.5](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.5) (31/05/17)

- Changed "name" to "label" under "executableBlocks/designCompetences/designCompetence"
- Enhanced "contactType" with a different structure and various fields
- Replaced "specialist" under "executableBlocks/designCompetences/designCompetence/metadata/generalInfo" with "owner" and "operator"
- Removed "standardDeviation" from "executableBlocks/designCompetences/designCompetence/metadata/performanceInfo"
- Added "doeTableOrder" and made "doeTable" optional (as a table is only required in case of method using a custom design table)
- Added "instances" under "architectureElements/parameters"
- Added "unit" and "dataType" for "parameters/parameter"
- Added "validRanges" to "inputsType"
- Added "type", "status", "creationDate" and "modelDefinition" to "executableBlocks/designCompetences/designCompetence/metadata/generalInfo"
- Added "verfications" to "executableBlocks/designCompetences/designCompetence/metadata/generalInfo"
- Completely restructured "executableBlocks/designCompetences/designCompetence/metadata/executionInfo" and added also "platformspecificInfo" under "executableBlocks/designCompetences/designCompetence/" which takes some of the elements that were initially located in "exectionInfo"
- Replaced "functions" under "executableBlocks" with "mathematicalFunctions" and completely reorganized the elements

## [0.4](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.4) (28/03/17)

- Replaced all xs:all types by xs:sequence (to improve the consistency and perspicuousness)
- Converted "problemDefinition/problemFormulation/executableBlocksOrder/..." from string with an (ordered) array to xs:sequence with position attributes where the position corresponds to the position of the element in the array. Slightly changed naming of the corresponding elements
- Converted "problemDefinition/parameters/designVariables/designVariable/samples" from string with an (ordered) array to xs:sequence with position attributes where the position corresponds to the position of the element in the array
- Converted "problemDefinition/problemRoles/executableBlocks/..." from string with an array to xs:sequence. Adjusted naming to camel case notation (for consistency)
- Added "preDesvars" and "postDesvars" to "problemDefinition/problemRoles/executableBlocks/..."
- Made "executableBlocks/designCompetences/designCompetence/metadata/performanceInfo" an optional element

## [0.3](https://gitlab.tudelft.nl/lr-fpp-mdo/cmdows/-/tree/master/schema/0.3) (24/03/17)

- First public release of CMDOWS